//= require jquery
//= require particles.js/particles.js
//= require bxslider-4/src/js/jquery.bxslider.js
//= require_tree .
$(document).ready(function(){
	var $window_width = parseInt($(window).width()),
            // $mobile = $window_width < 481,
            $pad_min = $window_width < 641,
            $pad = 640 < $window_width,
            $note = 769 < $window_width;

	$('.bg-gray').hover(function(){
		$(this).addClass('bg-coloranimation');
	},function(){
		$(this).removeClass('bg-coloranimation');
	});
	$('.i-slideDown').on('click',function(){
		$(this).toggleClass('active').parents('.schedule-body').next('.schedule-footer').toggleClass('active');
	});
	$('.mbl-date').on('click',function(){
		$('[data-schedule]').addClass('hidden-xs').eq($(this).index()).toggleClass('hidden-xs');
		$('.getSchdule div').toggleClass('active');
	});
	$('.mbl-date').on('click',function(){
		$('.mbl-date').toggleClass('active');
	});

	/* schedule
	----------------------------------*/
	$('.schedule-event').on('click', function(){
		$(this).find('.toggle-icon').toggleClass("schedule-on schedule-off")
	    $(this.parentElement).find('.schedule-detail').toggle();
	});
	$('#day_one_title').on('click', function(){
		$('#day_two_title').removeClass("active");
		$(this).addClass("active");
		$('#day_one_schedule').show();
		$('#day_two_schedule').hide();
		
	});
	$('#day_two_title').on('click', function(){
		$('#day_one_title').removeClass("active");
		$(this).addClass("active");
		$('#day_one_schedule').hide();
		$('#day_two_schedule').show();
	});

	/* particles
	----------------------------------*/
	particlesJS('particles-js', {
	  "particles": {
	    "number": {
	      "value": 160,
	      "density": {
	        "enable": true,
	        "value_area": 800
	      }
	    },
	    "color": {
	      "value": "#ffffff"
	    },
	    "shape": {
	      "type": "circle",
	      "stroke": {
	        "width": 0,
	        "color": "#000000"
	      },
	      "polygon": {
	        "nb_sides": 5
	      },
	      "image": {
	        "src": "img/github.svg",
	        "width": 100,
	        "height": 100
	      }
	    },
	    "opacity": {
	      "value": 1,
	      "random": true,
	      "anim": {
	        "enable": true,
	        "speed": 1,
	        "opacity_min": 0,
	        "sync": false
	      }
	    },
	    "size": {
	      "value": 3,
	      "random": true,
	      "anim": {
	        "enable": false,
	        "speed": 4,
	        "size_min": 0.3,
	        "sync": false
	      }
	    },
	    "line_linked": {
	      "enable": false,
	      "distance": 150,
	      "color": "#ffffff",
	      "opacity": 0.4,
	      "width": 1
	    },
	    "move": {
	      "enable": true,
	      "speed": 1,
	      "direction": "none",
	      "random": true,
	      "straight": false,
	      "out_mode": "out",
	      "bounce": false,
	      "attract": {
	        "enable": false,
	        "rotateX": 600,
	        "rotateY": 600
	      }
	    }
	  },
	  "interactivity": {
	    "detect_on": "canvas",
	    "events": {
	      "onhover": {
	        "enable": true,
	        "mode": "bubble"
	      },
	      "onclick": {
	        "enable": true,
	        "mode": "repulse"
	      },
	      "resize": true
	    },
	    "modes": {
	      "grab": {
	        "distance": 400,
	        "line_linked": {
	          "opacity": 1
	        }
	      },
	      "bubble": {
	        "distance": 250,
	        "size": 0,
	        "duration": 2,
	        "opacity": 0,
	        "speed": 3
	      },
	      "repulse": {
	        "distance": 400,
	        "duration": 0.4
	      },
	      "push": {
	        "particles_nb": 4
	      },
	      "remove": {
	        "particles_nb": 2
	      }
	    }
	  },
	  "retina_detect": true
	}, function() {
	  console.log('callback - particles.js config loaded');
	});
	/* bxSlider
	----------------------------------*/
	if ($note) {
		$('.sepeak-slider').bxSlider({
			minSlides: 3,
 			maxSlides: 3,
  			slideWidth: 320,
  			slideMargin: 30,
  			pager: false,
  			controls: true
		});
    } else if ($pad) {
        $('.sepeak-slider').bxSlider({
			minSlides: 2,
 			maxSlides: 2,
  			slideWidth: 320,
  			slideMargin: 30,
  			pager: false,
  			controls: true
  		});
    } else if ($pad_min) {
    	$('.sepeak-slider').bxSlider({
			minSlides: 1,
 			maxSlides: 1,
  			slideWidth: 320,
  			// slideMargin: 30,
  			pager: false,
  			controls: false
  		});
    }
	/* hover mask
	----------------------------------*/
	$('.speaker .mask').hide();
	$('.speaker-name a').on('click',
    	function () {
    		$(this).children('selector')
			$(this).parent('.speaker-name').prev('.speaker-info').children('.mask').toggle('500');
		});

	/* scroll
	----------------------------------*/
	$(document).scroll(function (fixT) {

        if ($(document).scrollTop() > 350 ) {
            $(".mbl-dates,.dkt-dates").addClass('fixTop');
        } else {
            $(".mbl-dates,.dkt-dates").removeClass('fixTop');
        }
    });
		
});